output "vpc_id" {
  value = aws_vpc.default.id
}
output "vpc_arn" {
  value = aws_vpc.default.arn
}
